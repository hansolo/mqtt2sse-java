/*
 * Copyright (c) 2015 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.mqtt2sse;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.IntStream;


@CrossOrigin
@RestController
@RequestMapping("")
public class MqttRestController {
    private static final Map<SseEmitter, String> SUBSCRIBERS  = new HashMap<>();
    private static final Map<String, Pattern>    PATTERNS     = new HashMap<>();
    private static final MqttManager             MQTT_MANAGER = new MqttManager();


    // ******************** Constructor ***************************************
    public MqttRestController() {
        MQTT_MANAGER.setOnMessageReceived(event -> {
            if (!SUBSCRIBERS.isEmpty()) {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("topic", event.TOPIC);
                jsonObj.put("message", event.MESSAGE.toString());
                forwardToSSE(event.TOPIC, jsonObj);
            }
        });
    }


     // ******************** REST Methods *************************************
    //@CrossOrigin
    @RequestMapping(path = "/publish", method = RequestMethod.POST)
    public @ResponseBody String publish(final @RequestBody() String JSON) throws IOException {
        JSONObject jsonObj  = (JSONObject) JSONValue.parse(JSON);
        String     topic    = jsonObj.get("topic").toString();
        String     message  = jsonObj.get("msg").toString();
        boolean    retained = Boolean.parseBoolean(jsonObj.get("retained").toString());
        int        qos      = Common.clamp(MqttManager.QOS_0, MqttManager.QOS_2, Integer.parseInt(jsonObj.get("qos").toString()));
        MQTT_MANAGER.publish(topic, qos, message, retained);
        return "Message successfully published";
    }

    //@CrossOrigin
    @RequestMapping(path = "/subscribe", method = RequestMethod.GET)
    public SseEmitter subscribe(final @RequestParam("topic") String TOPIC,
                                final @RequestParam("qos") int REQUESTED_QOS) throws IOException {
        final SseEmitter SUBSCRIBER = new SseEmitter();
        final int        QOS        = Common.clamp(MqttManager.QOS_0, MqttManager.QOS_2, REQUESTED_QOS);

        if (!SUBSCRIBERS.containsKey(SUBSCRIBER)) {
            SUBSCRIBERS.put(SUBSCRIBER, TOPIC);
            addPattern(TOPIC);
            MQTT_MANAGER.subscribe(TOPIC, QOS);
        }

        SUBSCRIBER.onCompletion(() -> {
            SUBSCRIBERS.remove(SUBSCRIBER);
            unSubscribeIfPossible(TOPIC);
        });

        return SUBSCRIBER;
    }


    // ******************** Methods *******************************************
    /**
     * Forwards a given MQTT message(wrapped in JSON) on a given TOPIC to all
     * subscribers via SSE.
     * @param TOPIC Name of the Mqtt topic
     * @param JSON
     */
    private void forwardToSSE(final String TOPIC, final JSONObject JSON) {
        SUBSCRIBERS.keySet().forEach(subscriber -> {
            final String  SUBSCRIBED_TOPIC   = SUBSCRIBERS.get(subscriber);
            final boolean MATCH_TOPIC        = SUBSCRIBED_TOPIC.equals(TOPIC); // exact same topic
            final boolean MATCH_SINGLE_LEVEL = PATTERNS.get(SUBSCRIBED_TOPIC).matcher(TOPIC).matches();
            final boolean MATCH_MULTI_LEVEL  = TOPIC.startsWith(SUBSCRIBED_TOPIC.endsWith("#") ? SUBSCRIBED_TOPIC.substring(0, SUBSCRIBED_TOPIC.indexOf("#")) : SUBSCRIBED_TOPIC);

            // Update those clients that subscribed to the topic of the current message
            if (MATCH_TOPIC || MATCH_SINGLE_LEVEL || MATCH_MULTI_LEVEL) {
                try {
                    subscriber.send(JSON.toJSONString(), MediaType.APPLICATION_JSON);
                } catch (IOException e) {
                    subscriber.complete();
                    SUBSCRIBERS.remove(subscriber);
                }
            }
        });
    }

    /**
     * Creates a RegEx pattern from the given topic. The pattern will be added
     * to the PATTERNS map if it's not already in there and will be returned.
     * @param TOPIC MQTT Topic
     * @return The RegEx pattern for the given TOPIC
     */
    private Pattern addPattern(final String TOPIC) {
        if (PATTERNS.containsKey(TOPIC)) return PATTERNS.get(TOPIC);
        final String[]      SUB_TOPICS      = TOPIC.split("/");
        final StringBuilder PATTERN_BUILDER = new StringBuilder();
        IntStream.range(0, SUB_TOPICS.length).forEach(i -> PATTERN_BUILDER.append(i == 0 ? "" : "\\/")
                                                                          .append("(")
                                                                          .append(SUB_TOPICS[i].equals("+") ? "(((\\+)|(\\w)*))" : SUB_TOPICS[i])
                                                                          .append(")"));
        Pattern PATTERN = Pattern.compile(PATTERN_BUILDER.toString());
        PATTERNS.put(TOPIC, PATTERN);
        return PATTERN;
    }

    /**
     * Unsubscribes from given TOPIC if no one else is subscribed to it
     * @param TOPIC MQTT Topic
     */
    private void unSubscribeIfPossible(final String TOPIC) {
        if (SUBSCRIBERS.values().contains(TOPIC)) return;
        MQTT_MANAGER.unSubscribe(TOPIC);
    }
}
